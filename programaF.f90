program main

    implicit none   
    
    real(8), dimension (:,:), allocatable :: matriz
    real(8), dimension (:), allocatable :: vetor, produto
    integer :: n, nMax=40000, linha, coluna, incremento = 100
    REAL(8) :: randNum, inicio, fim

    open(1, file="linhaColuna02.txt", status="new")
    open(2, file="ColunaLinha02.txt", status="new")

    do n = 100, nMax, incremento

        allocate (matriz(n,n))
        allocate (vetor(n))
        allocate (produto(n))      


        do linha = 1, n   
            CALL RANDOM_NUMBER(randNum) 
            vetor = randNum   
            produto(linha) = 0     
            do coluna = 1, n 
                CALL RANDOM_NUMBER(randNum)               
                matriz(linha, coluna) = randNum        
            end do      
        end do 
        
        call cpu_time(inicio)
        do linha = 1, n         
            do coluna = 1, n 
                produto(linha) = produto(linha) + matriz(linha, coluna)*vetor(coluna)     
            end do      
        end do 
        call cpu_time(fim)
        
        write(1,"(I6, A, F12.8)") n, ";", fim-inicio

        call cpu_time(inicio)
        do coluna = 1, n         
            do linha = 1, n 
                produto(linha) = produto(linha) + matriz(linha, coluna)*vetor(coluna)     
            end do      
        end do 
        call cpu_time(fim)

        write(2,"(I6, A, F12.8)") n, ";", fim-inicio

        deallocate (matriz)
        deallocate (vetor)
        deallocate (produto)

    end do

    close(1)
    close(2)
end program main
