#%%
import pandas as pd 
#%%
dict = {0: 'N', 1:'Tempo (s)'}
dfLinhaColuna = pd.read_csv('linhaColuna02.txt', delimiter=';', header=None)
dfLinhaColuna.rename(columns = dict, inplace=True) 
dfLinhaColuna

#%%
dict = {0: 'N', 1:'Tempo (s)'}
dfColunaLinha = pd.read_csv('colunaLinha02.txt', delimiter=';', header=None)
dfColunaLinha.rename(columns = dict, inplace=True) 
dfColunaLinha
# %%
import matplotlib.pyplot as plt 
fig = plt.figure(figsize=(10,10))

plt.plot(dfLinhaColuna['Tempo (s)'], dfLinhaColuna['N'], label='Linha Coluna')
plt.plot(dfColunaLinha['Tempo (s)'], dfColunaLinha['N'], label='Coluna Linha')

plt.xlabel('Tempo (s)')
plt.ylabel('N')

plt.title("Fortran - N x Tempo")

plt.legend()

plt.savefig('grafico.jpg')
plt.show()
